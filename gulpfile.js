const { src, dest, series, parallel, watch } = require('gulp');
const postcss = require('gulp-postcss');
const babel = require('gulp-babel');
const sync = require('browser-sync');
const del = require('del');

// HTML

function html() {
  return src('./app/*.html')
    .pipe(dest('dist'))
    .pipe(sync.stream())
}

// Styles

function styles() {
  return src('./app/styles/style.css')
    .pipe(postcss([
      require('postcss-import'),
      require('autoprefixer'),
      require('css-mqpacker'),
      require('postcss-csso')({
        comments: false
      })
    ]))
    .pipe(dest('dist'))
    .pipe(sync.stream())
}

// Scripts

function scripts() {
  return src('./app/scripts/index.js')
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(dest('dist'))
    .pipe(sync.stream())
}

// Transfer

function transfer() {
  return src([
    './app/fonts/**/*',
    './app/images/**/*'
  ], { base: 'app' })
    .pipe(dest('dist'))
    .pipe(sync.stream({
      once: true
    }))
}

// Server

function server() {
  sync.init({
    ui: false,
    notify: false,
    server: {
      baseDir: 'dist'
    }
  })
}

// Watcher

function watcher() {
  watch('./app/*.html', series(html));
  watch('./app/styles/**/**/*', series(styles));
  watch('./app/scripts/**/*', series(scripts));
  watch([
    './app/images/**/*',
    './app/fonts/**/*'
  ], series(transfer));
}

// Clean

function clean(){
  return del('dist')
}
exports.clean = clean;

exports.default = series(html, styles, scripts, transfer, parallel(watcher, server));